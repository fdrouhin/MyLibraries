package models ;

import javax.persistence.*;
import com.avaje.ebean.Model;
import play.data.validation.*;

/**
 * A book
 */
 @Entity
public class Book extends Model {
    
    /**
     * ID for database
     */
    @Id
    public Long id ;

    /**
     * Title
     */
    @Constraints.Required
    public String title ;
    
    /**
     * Summary
     */
    public String summary ;
     
     /**
      * Author
      */
    @Constraints.Required
    public String author ;
    
    //@Column(name="OWNER_ID")
    //public Long ownerId ;
    
    /**
     * Library attached to Book
     */
    @ManyToOne
    public Library library ;

    /**
     * Access to database
     */
    public static Finder<Long, Book> find = new Finder<Long,Book>(Book.class);

     /**
      * set the library for this book
      */
    public void setLibrary(Library library) {  
        if (library == null || library.id == null) {
		    throw new IllegalArgumentException("Library or library id is empty, please report this bug to developper") ;
        }							       
										       
		this.library = library ;
    } 
}
