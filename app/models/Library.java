package models ;

import javax.persistence.*;
import com.avaje.ebean.Model;
import play.data.validation.*;

import java.util.* ;

/**
 * A library
 */
 @Entity
public class Library extends Model {
    
    /**
     * ID for database
     */
    @Id
    //@Column(name="EMP_ID")
    public Long id ;

    /**
     * Title
     */
    @Constraints.Required
    public String name ;
    
    /**
     * Description
     */
    public String description ;
    
    /**
     * List of books
     */
    //@JoinColumn(name="OWNER_ID", referencedColumnName="EMP_ID")
    @OneToMany(cascade = CascadeType.ALL)
    public List<Book> books ;

    /**
     * Access to database
     */
    public static Finder<Long, Library> find = new Finder<Long,Library>(Library.class);

     /**
      * Add new new book
      */
     public Book addBook(Book book) {
        books.add(book) ;
		book.setLibrary(this) ;
		return book ;
     }
}
