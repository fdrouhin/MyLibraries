package controllers;

import play.mvc.* ;
import play.data.* ;

import play.i18n.Messages;
import javax.inject.Inject ;

import java.util.List ;

import views.html.library.*;
import views.html.book.*;
import views.html.* ;


import models.Library ;
import models.Book ;

import play.Logger;

import java.lang.Process ;
import java.lang.Runtime ;
import java.io.IOException ;
import java.io.BufferedReader ;
import java.io.InputStreamReader ;
import java.lang.InterruptedException ;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page for Library models
 */
public class LibraryController extends Controller {

    /** Inject a Form factory
     */
    @Inject FormFactory formFactory;
    
    /** General book form
     */
    Form<Library> libraryForm ;
    
    /** General book form
     */
    Form<Book> bookForm ;
     
    /**
     * Get a library form
     */
    public Form<Library> getLibraryForm() {
        if (libraryForm == null) libraryForm = formFactory.form(Library.class) ;
        return libraryForm ;
    }
    
    /**
     * Get a book form
     */
    public Form<Book> getBookForm() {
        if (bookForm == null) bookForm = formFactory.form(Book.class) ;
        return bookForm ;
    }

    /**
     * Display all libraries
     */
    public Result index() {
        List<Library> libs = Library.find.all() ;
        Logger.info("Found " + libs.size() + " libraries") ;
        return ok(libraries.render("All Libraries", libs));
    }
    
    /**
     * Display library form
     */
    public Result create() {
        return ok(create.render(getLibraryForm())) ;
    }
    
    /**
     * Save the new library
     */
    public Result save() {
        Form<Library> libForm = getLibraryForm().bindFromRequest() ;
	    if (libForm.hasErrors()) {
		    return badRequest(create.render(libForm)) ;
	    }
	    else {
		    Library library = libForm.bindFromRequest().get() ;
		    library.save() ;
		    
            //flash("success", Messages.get("library.created", library.name));

            return redirect(routes.LibraryController.index()) ;
	    }
    }  
    
    /**
     * Update the library (name and description)
     */
    public Result updateForm(Long id) {
        Library library = Library.find.byId(id) ;
	    
	    Form<Library> libForm = getLibraryForm() ;
	    libForm = libForm.fill(library);
	    
    	return ok(update_form.render(libForm, library)) ;
    }
    
    /**
     * Update the library
     */
    public Result update(Long id) {
        Form<Library> libForm = getLibraryForm().bindFromRequest() ;
	    if (libForm.hasErrors()) {
	        Library library = Library.find.byId(id) ;
		    return badRequest(update_form.render(libForm, library)) ;
	    }
	    else {
		    Library library = libForm.bindFromRequest().get() ;
		    library.update() ;
		    
            return redirect(routes.LibraryController.index()) ;
	    }
    }
    
    /**
     * Delete a book
     */ 
    public Result delete(Long id) {
        Library library = Library.find.byId(id) ;
        library.delete() ;
        return redirect(routes.LibraryController.index()) ;
    }   
    
    /**
     * Show a specific library
     */
    public Result show(Long id) {
        return redirect(routes.LibraryController.indexlibrary(id)) ;
    }
    
    /**
     * Save the book done by the form
     */
    public Result savebook(Long libId) {
        
        // retreive library
        Library library = Library.find.byId(libId) ;
        if (library == null || library.id == null || library.id != libId) {
	        return badRequest(error.render("Library ID " + libId + " not existing")) ;
	    }
        
        // book submitted
        Form<Book> bForm = getBookForm().bindFromRequest() ;
	    if (bForm.hasErrors()) {
		    return badRequest(createbook.render(bForm, library)) ;
	    }
	    else {
	        Book book = bForm.bindFromRequest().get() ;
	       
		    library.addBook(book) ;
		    library.save() ;
            
		    Library lib = Library.find.byId(libId) ;
		    Logger.info("Number of books in library " + library.books.size() + " / " + lib.books.size()) ;
		    
            return redirect(routes.LibraryController.indexlibrary(library.id)) ;
	    }
    }
    
    /**
     * Display all libraries
     */
    public Result indexlibrary(Long id) {
        Library library = Library.find.byId(id) ;
        if (library != null) {
            return ok(books.render(library));
        }
        else {
            return redirect(routes.LibraryController.index()) ;
        }
    }
    
    /**
     * Create a new book in library
     */
    public Result createbook(Long libId) {
        Library library = Library.find.byId(libId) ;
        return ok(createbook.render(getBookForm(), library)) ;
     }
}