# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table book (
  id                            bigint not null,
  title                         varchar(255),
  summary                       varchar(255),
  author                        varchar(255),
  library_id                    bigint,
  constraint pk_book primary key (id)
);
create sequence book_seq;

create table library (
  id                            bigint not null,
  name                          varchar(255),
  description                   varchar(255),
  constraint pk_library primary key (id)
);
create sequence library_seq;

alter table book add constraint fk_book_library_id foreign key (library_id) references library (id) on delete restrict on update restrict;
create index ix_book_library_id on book (library_id);


# --- !Downs

alter table book drop constraint if exists fk_book_library_id;
drop index if exists ix_book_library_id;

drop table if exists book;
drop sequence if exists book_seq;

drop table if exists library;
drop sequence if exists library_seq;

